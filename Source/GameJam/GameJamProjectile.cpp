// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameJamProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"

AGameJamProjectile::AGameJamProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AGameJamProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;


	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

void AGameJamProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		URadialForceComponent* RadialForce = NewObject<URadialForceComponent>(this, TEXT("ForceComp"));

		// Set Radial Force attributes
		RadialForce->DestructibleDamage = 1000.f;
		RadialForce->Radius = 1000.f;
		RadialForce->ForceStrength = 15000.f;
		RadialForce->ImpulseStrength = 15000.f;
		RadialForce->bIgnoreOwningActor = true;

		RadialForce->SetWorldLocation(GetActorLocation());
		RadialForce->AttachTo(this->GetRootComponent(), NAME_None, EAttachLocation::KeepWorldPosition);
		RadialForce->FireImpulse();
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

		RadialForce->DestroyComponent(true);
		Destroy();
	}
	URadialForceComponent* RadialForce = NewObject<URadialForceComponent>(this, TEXT("ForceComp"));

	// Set Radial Force attributes
	RadialForce->DestructibleDamage = 1000.f;
	RadialForce->Radius = 1000.f;
	RadialForce->ForceStrength = 15000.f;
	RadialForce->ImpulseStrength = 15000.f;
	RadialForce->bIgnoreOwningActor = true;

	RadialForce->SetWorldLocation(GetActorLocation());
	RadialForce->AttachTo(this->GetRootComponent(), NAME_None, EAttachLocation::KeepWorldPosition);
	RadialForce->FireImpulse();
	RadialForce->DestroyComponent(true);
	Destroy();
}